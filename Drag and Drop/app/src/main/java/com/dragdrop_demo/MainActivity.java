package com.dragdrop_demo;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnDragListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ImageView imageView, imageView1, imageView2, imageView3, imageView4, imageView5,
    imageView6, imageView7,imageView8, imageBar, imageCarboBar, imageProteinBar, imageSugarBar, imageFatsBar,
    imageView9, imageView10;
    private TextView label, counter_cal, counter_carb, counter_protein, counter_sugar, counter_fats;
    private static final String IMAGE_VIEW_TAG = "Broccoli";
    private static final String IMAGE_VIEW_TAG1 = "Carrot";
    private static final String IMAGE_VIEW_TAG2 = "Cucumber";
    private static final String IMAGE_VIEW_TAG4 = "Olive";
    private static final String IMAGE_VIEW_TAG5 = "Onion";
    private static final String IMAGE_VIEW_TAG6 = "Potato";
    private static final String IMAGE_VIEW_TAG7 = "Red Onion";
    private static final String IMAGE_VIEW_TAG8 = "Tomato";
    private static final String IMAGE_VIEW_TAG9 = "Balsamic Vinegar";
    private static final String IMAGE_VIEW_TAG10 = "Honey Mustard";
    public static ArrayList<String> ingredientsInBowl = new ArrayList<>();
    public static ArrayList<ArrayList<Float>> savedResult = new ArrayList<ArrayList<Float>>();
    ArrayList<Float> savedEntry = new ArrayList<Float>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        implementEvents();
        label = (TextView) findViewById(R.id.label_1);
        counter_cal = (TextView) findViewById(R.id.cal_counter);
        counter_carb = (TextView) findViewById(R.id.carb_counter);
        counter_protein = (TextView) findViewById(R.id.protein_counter);
        counter_sugar = (TextView) findViewById(R.id.sugar_counter);
        counter_fats = (TextView) findViewById(R.id.fats_counter);
        imageBar = (ImageView) findViewById(R.id.imageCalBar);
        imageCarboBar = (ImageView) findViewById(R.id.imageCarbBar);
        imageProteinBar = (ImageView) findViewById(R.id.imageProteinBar);
        imageSugarBar = (ImageView) findViewById(R.id.imageSugarBar);
        imageFatsBar = (ImageView) findViewById(R.id.imageFatBar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void onResume() {
        super.onResume();
        imageBar.requestLayout();
        imageBar.getLayoutParams().width = 0;
        imageCarboBar.requestLayout();
        imageCarboBar.getLayoutParams().width = 0;
        imageProteinBar.requestLayout();
        imageProteinBar.getLayoutParams().width = 0;
        imageSugarBar.requestLayout();
        imageSugarBar.getLayoutParams().width = 0;
        imageFatsBar.requestLayout();
        imageFatsBar.getLayoutParams().width = 0;

            for(int i = 0; i < ingredientsInBowl.size(); i++){
                if(ingredientsInBowl.get(i).equals("Broccoli")){
                    setBroccoli();
                }else if(ingredientsInBowl.get(i).equals("Carrot")){
                    setCarrot();
                }else if(ingredientsInBowl.get(i).equals("Cucumber")){
                    setCucumber();
                }else if(ingredientsInBowl.get(i).equals("Olive")){
                    setOlives();
                }else if(ingredientsInBowl.get(i).equals("Onion")){
                    setOnions();
                }else if(ingredientsInBowl.get(i).equals("Potato")){
                    setPotatoes();
                }else if(ingredientsInBowl.get(i).equals("Red Onion")){
                    setOnions();
                }else if(ingredientsInBowl.get(i).equals("Tomato")){
                    setTomatoes();
                }else if(ingredientsInBowl.get(i).equals("Balsamic Vinegar")){
                    setVinegar();
                }else if(ingredientsInBowl.get(i).equals("Honey Mustard")){
                    setMustard();
                }
        }

        counter_cal.setText(" " +imageBar.getLayoutParams().width + "kcal");
        counter_carb.setText(" " +imageCarboBar.getLayoutParams().width + "g");
        counter_protein.setText(" " +imageProteinBar.getLayoutParams().width + "g");
        counter_sugar.setText(" " +imageSugarBar.getLayoutParams().width + "g");
        counter_fats.setText(" " +imageFatsBar.getLayoutParams().width + "g");
    }

    //show the new and save icon on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.commonmenus, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //give function to new and save
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //input save
        int id = item.getItemId();

        if (id == R.id.menuSave) {
            if(imageCarboBar.getLayoutParams().width == 0 && imageBar.getLayoutParams().width == 0 && imageProteinBar.getLayoutParams().width == 0
                    && imageSugarBar.getLayoutParams().width == 0 && imageFatsBar.getLayoutParams().width == 0){
                label.setText("Please Add item in Bowl");
            }else {
                savedEntry = new ArrayList<Float>();
                savedEntry.add((float) imageBar.getLayoutParams().width);
                savedEntry.add((float) imageCarboBar.getLayoutParams().width);
                savedEntry.add((float) imageProteinBar.getLayoutParams().width);
                savedEntry.add((float) imageSugarBar.getLayoutParams().width);
                savedEntry.add((float) imageFatsBar.getLayoutParams().width);

                savedResult.add(savedEntry);
                label.setText("Salad bowl Saved");
            }


            counter_cal.setText(" " +imageBar.getLayoutParams().width + "kcal");
            counter_carb.setText(" " +imageCarboBar.getLayoutParams().width + "g");
            counter_protein.setText(" " +imageProteinBar.getLayoutParams().width + "g");
            counter_sugar.setText(" " +imageSugarBar.getLayoutParams().width + "g");
            counter_fats.setText(" " +imageFatsBar.getLayoutParams().width + "g");
        }
        else if (id == R.id.menuNew) {
            imageBar.requestLayout();
            imageBar.getLayoutParams().width = 0;
            imageCarboBar.requestLayout();
            imageCarboBar.getLayoutParams().width = 0;
            imageProteinBar.requestLayout();
            imageProteinBar.getLayoutParams().width = 0;
            imageSugarBar.requestLayout();
            imageSugarBar.getLayoutParams().width = 0;
            imageFatsBar.requestLayout();
            imageFatsBar.getLayoutParams().width = 0;

            ingredientsInBowl.clear();

            counter_cal.setText(" " +imageBar.getLayoutParams().width + "kcal");
            counter_carb.setText(" " +imageCarboBar.getLayoutParams().width + "g");
            counter_protein.setText(" " +imageProteinBar.getLayoutParams().width + "g");
            counter_sugar.setText(" " +imageSugarBar.getLayoutParams().width + "g");
            counter_fats.setText(" " +imageFatsBar.getLayoutParams().width + "g");
            label.setText("New Bowl Created");
        }

        return super.onOptionsItemSelected(item);
    }

    public void ingredientList(View view){
        Intent intent_Ingredient = new Intent(MainActivity.this, IngredientActivity.class);
        startActivity(intent_Ingredient);
    }


    //Find all views and set Tag to all draggable views
    private void findViews() {
        imageView = (ImageView) findViewById(R.id.image_view);
        imageView.setTag(IMAGE_VIEW_TAG);
        imageView1 = (ImageView) findViewById(R.id.image_view1);
        imageView1.setTag(IMAGE_VIEW_TAG1);
        imageView2 = (ImageView) findViewById(R.id.image_view2);
        imageView2.setTag(IMAGE_VIEW_TAG2);
        imageView4 = (ImageView) findViewById(R.id.image_view4);
        imageView4.setTag(IMAGE_VIEW_TAG4);
        imageView5 = (ImageView) findViewById(R.id.image_view5);
        imageView5.setTag(IMAGE_VIEW_TAG5);
        imageView6 = (ImageView) findViewById(R.id.image_view6);
        imageView6.setTag(IMAGE_VIEW_TAG6);
        imageView7 = (ImageView) findViewById(R.id.image_view7);
        imageView7.setTag(IMAGE_VIEW_TAG7);
        imageView8 = (ImageView) findViewById(R.id.image_view8);
        imageView8.setTag(IMAGE_VIEW_TAG8);
        imageView9 = (ImageView) findViewById(R.id.image_view9);
        imageView9.setTag(IMAGE_VIEW_TAG9);
        imageView10 = (ImageView) findViewById(R.id.image_view10);
        imageView10.setTag(IMAGE_VIEW_TAG10);
    }


    //Implement long click and drag listener
    private void implementEvents() {
        imageView.setOnTouchListener(new ChoiceTouchListener());
        imageView1.setOnTouchListener(new ChoiceTouchListener());
        imageView2.setOnTouchListener(new ChoiceTouchListener());
        imageView4.setOnTouchListener(new ChoiceTouchListener());
        imageView5.setOnTouchListener(new ChoiceTouchListener());
        imageView6.setOnTouchListener(new ChoiceTouchListener());
        imageView7.setOnTouchListener(new ChoiceTouchListener());
        imageView8.setOnTouchListener(new ChoiceTouchListener());
        imageView9.setOnTouchListener(new ChoiceTouchListener());
        imageView10.setOnTouchListener(new ChoiceTouchListener());

        findViewById(R.id.left_layout).setOnDragListener(this);
    }


    private final class ChoiceTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {

                ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());

                String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};

                ClipData data = new ClipData(view.getTag().toString(), mimeTypes, item);

                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data, shadowBuilder, view, 0);
                return true;
            } else {
                return false;
            }
        }
    }

    // This is the method that the system calls when it dispatches a drag event to the
    // listener.
    @Override
    public boolean onDrag(View view, DragEvent event) {
        // Defines a variable to store the action type for the incoming event
        int action = event.getAction();
        // Handles each of the expected events
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                // Determines if this View can accept the dragged data
                if (event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    return true;
                }

                // Returns false. During the current drag and drop operation, this View will
                // not receive events again until ACTION_DRAG_ENDED is sent.
                return false;

            case DragEvent.ACTION_DRAG_ENTERED:

                // Invalidate the view to force a redraw in the new tint
                view.invalidate();

                return true;
            case DragEvent.ACTION_DRAG_LOCATION:
                // Ignore the event
                return true;
            case DragEvent.ACTION_DRAG_EXITED:
                view.getBackground().clearColorFilter();
                view.invalidate();

                return true;
            case DragEvent.ACTION_DROP:
                // Gets the item containing the dragged data
                ClipData.Item item = event.getClipData().getItemAt(0);

                // Gets the text data from the item.
                String dragData = item.getText().toString();
                label.setText(dragData + " added");
                ingredientsInBowl.add(dragData);

                if(dragData.equals("Broccoli")){
                    setBroccoli();
                }else if(dragData.equals("Carrot")){
                   setCarrot();
                }else if(dragData.equals("Cucumber")){
                    setCucumber();
                }else if(dragData.equals("Olive")){
                    setOlives();
                }else if(dragData.equals("Onion")){
                    setOnions();
                }else if(dragData.equals("Potato")){
                    setPotatoes();
                }else if(dragData.equals("Red Onion")){
                    setOnions();
                }else if(dragData.equals("Tomato")){
                    setTomatoes();
                }else if(dragData.equals("Balsamic Vinegar")){
                    setVinegar();
                }else if(dragData.equals("Honey Mustard")){
                    setMustard();
                }

                counter_cal.setText(" " +imageBar.getLayoutParams().width + "kcal");
                counter_carb.setText(" " +imageCarboBar.getLayoutParams().width + "g");
                counter_protein.setText(" " +imageProteinBar.getLayoutParams().width + "g");
                counter_sugar.setText(" " +imageSugarBar.getLayoutParams().width + "g");
                counter_fats.setText(" " +imageFatsBar.getLayoutParams().width + "g");

                view.getBackground().clearColorFilter();

                // Invalidates the view to force a redraw
                view.invalidate();

                View v = (View) event.getLocalState();
                LinearLayout container = (LinearLayout) view;
                v.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE

                return true;
            case DragEvent.ACTION_DRAG_ENDED:
                view.getBackground().clearColorFilter();

                view.invalidate();

                return true;

            // An unknown action type was received.
            default:
                Log.e("Salad Bowl", "Unknown action type received by OnDragListener.");
                break;
        }
        return false;
    }
    public void setBroccoli(){
        imageBar.requestLayout();
        imageBar.getLayoutParams().width += 7;

        imageCarboBar.requestLayout();
        imageCarboBar.getLayoutParams().width += 3;

        imageProteinBar.requestLayout();
        imageProteinBar.getLayoutParams().width += 1;

        imageSugarBar.requestLayout();
        imageSugarBar.getLayoutParams().width += 1;

        imageFatsBar.requestLayout();
        imageFatsBar.getLayoutParams().width += 0;
    }
    public void setCarrot(){
        imageBar.requestLayout();
        imageBar.getLayoutParams().width += 7;

        imageCarboBar.requestLayout();
        imageCarboBar.getLayoutParams().width += 3;

        imageProteinBar.requestLayout();
        imageProteinBar.getLayoutParams().width += 0;

        imageSugarBar.requestLayout();
        imageSugarBar.getLayoutParams().width += 2;

        imageFatsBar.requestLayout();
        imageFatsBar.getLayoutParams().width += 0;
    }
    public void setCucumber(){
        imageBar.requestLayout();
        imageBar.getLayoutParams().width += 2;

        imageCarboBar.requestLayout();
        imageCarboBar.getLayoutParams().width += 1;

        imageProteinBar.requestLayout();
        imageProteinBar.getLayoutParams().width += 0;

        imageSugarBar.requestLayout();
        imageSugarBar.getLayoutParams().width += 0;

        imageFatsBar.requestLayout();
        imageFatsBar.getLayoutParams().width += 0;

    }
    public void setOlives(){
        imageBar.requestLayout();
        imageBar.getLayoutParams().width += 6;

        imageCarboBar.requestLayout();
        imageCarboBar.getLayoutParams().width += 1;

        imageProteinBar.requestLayout();
        imageProteinBar.getLayoutParams().width += 0;

        imageSugarBar.requestLayout();
        imageSugarBar.getLayoutParams().width += 0;

        imageFatsBar.requestLayout();
        imageFatsBar.getLayoutParams().width += 1;
    }
    public void setOnions(){
        imageBar.requestLayout();
        imageBar.getLayoutParams().width += 6;

        imageCarboBar.requestLayout();
        imageCarboBar.getLayoutParams().width += 3;

        imageProteinBar.requestLayout();
        imageProteinBar.getLayoutParams().width += 0;

        imageSugarBar.requestLayout();
        imageSugarBar.getLayoutParams().width += 1;

        imageFatsBar.requestLayout();
        imageFatsBar.getLayoutParams().width += 0;

    }
    public void setPotatoes(){
        imageBar.requestLayout();
        imageBar.getLayoutParams().width += 33;

        imageCarboBar.requestLayout();
        imageCarboBar.getLayoutParams().width += 16;

        imageProteinBar.requestLayout();
        imageProteinBar.getLayoutParams().width += 1;

        imageSugarBar.requestLayout();
        imageSugarBar.getLayoutParams().width += 1;

        imageFatsBar.requestLayout();
        imageFatsBar.getLayoutParams().width += 0;

    }
    public void setTomatoes(){
        imageBar.requestLayout();
        imageBar.getLayoutParams().width += 3;

        imageCarboBar.requestLayout();
        imageCarboBar.getLayoutParams().width += 2;

        imageProteinBar.requestLayout();
        imageProteinBar.getLayoutParams().width += 0;

        imageSugarBar.requestLayout();
        imageSugarBar.getLayoutParams().width += 1;

        imageFatsBar.requestLayout();
        imageFatsBar.getLayoutParams().width += 0;

    }
    public void setVinegar(){
        imageBar.requestLayout();
        imageBar.getLayoutParams().width += 56;

        imageCarboBar.requestLayout();
        imageCarboBar.getLayoutParams().width += 11;

        imageProteinBar.requestLayout();
        imageProteinBar.getLayoutParams().width += 0;

        imageSugarBar.requestLayout();
        imageSugarBar.getLayoutParams().width += 10;

        imageFatsBar.requestLayout();
        imageFatsBar.getLayoutParams().width += 0;

    }
    public void setMustard(){
        imageBar.requestLayout();
        imageBar.getLayoutParams().width += 100;

        imageCarboBar.requestLayout();
        imageCarboBar.getLayoutParams().width += 15;

        imageProteinBar.requestLayout();
        imageProteinBar.getLayoutParams().width += 1;

        imageSugarBar.requestLayout();
        imageSugarBar.getLayoutParams().width += 10;

        imageFatsBar.requestLayout();
        imageFatsBar.getLayoutParams().width += 26;

    }
}
