package com.dragdrop_demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private Button createbutton;
    private Button resultbutton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        createbutton = (Button) findViewById(R.id.create_button);
        resultbutton = (Button) findViewById(R.id.result_button);
//        findViews();
    }

    public void creating_next(View view){
        Intent intent_create = new Intent(HomeActivity.this, MainActivity.class);
        startActivity(intent_create);
    }

    public void result_next(View view){
        Intent intent_result = new Intent(HomeActivity.this, ResultActivity.class);
        startActivity(intent_result);
    }

//    protected void findViews() {
//        createbutton = (Button) findViewById(R.id.create_button);
//        createbutton = (Button) findViewById(R.id.result_button);
//    }



}

