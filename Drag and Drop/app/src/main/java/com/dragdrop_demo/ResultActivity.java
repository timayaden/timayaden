package com.dragdrop_demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    BarChart barChart;
    String[] labels = {"calories", "Carbohydrate", "Protein", "Sugar", "Fats"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Float cal = 0f, carb = 0f, pro = 0f, sug = 0f, fat = 0f;

        if(MainActivity.savedResult.size() != 0) {
            for (int i = 0; i < MainActivity.savedResult.size(); i++) {
                cal += MainActivity.savedResult.get(i).get(0);
                carb += MainActivity.savedResult.get(i).get(1);
                pro += MainActivity.savedResult.get(i).get(2);
                sug += MainActivity.savedResult.get(i).get(3);
                fat += MainActivity.savedResult.get(i).get(4);
            }

            cal /= MainActivity.savedResult.size();
            carb /= MainActivity.savedResult.size();
            pro /= MainActivity.savedResult.size();
            sug /= MainActivity.savedResult.size();
            fat /= MainActivity.savedResult.size();
        }

        barChart = (BarChart) findViewById(R.id.bargraph);

        ArrayList<BarEntry> barEntries = new ArrayList();
        barEntries.add(new BarEntry(0f, cal));
        barEntries.add(new BarEntry(1f, carb));
        barEntries.add(new BarEntry(2f, pro));
        barEntries.add(new BarEntry(3f, sug));
        barEntries.add(new BarEntry(4f, fat));
        BarDataSet barDataSet = new BarDataSet(barEntries, "");

        barDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);

        barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));

        BarData theData = new BarData(barDataSet);
        theData.setBarWidth(0.9f);
        barChart.setData(theData);
        barChart.setFitBars(true);
        barChart.getDescription().setEnabled(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }



}
